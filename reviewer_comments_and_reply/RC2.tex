\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{fourier}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{float}
\usepackage{color}

\author{Sven Karsten}
\title{Reply on RC2}

\begin{document}

\maketitle

In the following we address the reviewers concerns point by point by first repeating the comment in italics followed by our reply and a description of changes performed in the revised manuscript in \textcolor{red}{red}.

\section{Minor clarifications needed}

\textit{``l 189: I am surprised that ocean velocity is not coupled. Could you give some more justification for this. Adding these should not have a big impact on the run time of the model. Could you provide some numbers to explain why the expected error is negligible.''}\\
\\
Since tides do not play a significant role for the Baltic Sea, the velocity components of the ocean's surface are in the order of $10^{-2}$ to $10^{-1}$m/s and the atmospheric wind speed is typically one to three orders of magnitude larger. Hence the relative velocity of the wind to the water surface can be safely approximated by the wind speed only.

On the other hand, to include the water velocity in the flux formulas would be indeed possible by sending this information from the ocean model to the flux calculator. However, for historical reasons, we started the implementation of the flux calculation with the formulas implemented in the CCLM, where the water velocity components are simply neglected.\\
\\
\textcolor{red}{
We add the comparison between the order of magnitudes of the different velocities to the revised manuscript and note that tides are not taken into account.
}\\
\\
\\
\textit{``l 197-199: It is not clear to me what is happening with the radiation fluxes w.r.t. the exchange grid. I stumbled over the phrase “do not simply depend on”. Perhaps you could rephrase this paragraph to make it better understandable.''}\\
\\
To our best knowledge, the calculation of downward radiation fluxes cannot be reasonably implemented via bulk formulas that take only surface fields as input. Thus, the calculation of such fluxes is currently out of scope of the flux calculator's capabilities which uses bulk formulas to calculate mass, momentum and heat fluxes. Consequently the downward radiation fluxes are still entirely calculated by the atmospheric model and then sent via the flux calculator to the ocean model.\\
\\
\textcolor{red}{We rephrase the corresponding paragraph in Sect.~\textit{2.1 The exchange grid and the flux calculator} to meet the wording given above.}\\
\\
\\
\textit{``l 220: Please clarify in the text if atmosphere coupling fields are averaged in time over one coupling interval.''}\\
\\
The coupling fields sent from the oceanic and atmospheric models to the flux calculator are instantaneous fields and hence not averaged over a coupling time step. I might be a future feature to be able to switch between instantaneous and averaged fields and to investigate the impact of both variants.\\
\\
\textcolor{red}{In the revised manuscript we state in Sect.~\textit{2.3.1 Coupling cycle} that the fields are not averaged over time and give a perspective for future investigations.}\\
\\
\\
\textit{``Either as part of the introduction or if you decide for adding a discussion section: Could you indicate how your flux calculator approach differs from the “Flux Coupler” in CESM2.1: https://www.cesm.ucar.edu/models/cpl which is also programmed to calculate fluxes.''}\\
\\
The approach used in the CESM2.1 model is indeed very similar to our flux calculator. However, in contrast to our regional model, the CESM is designed for for global climate modeling as it is also part of the CMIP6 ensemble.\\
\\
\textcolor{red}{We thank the reviewer for pointing out the similarity between the coupling approaches employed in our model and in the CESM and added a corresponding sentence and citation in the introduction.}

\section{Technical corrections}

\textit{``l 63 and elsewhere in the text: suggestion to a use some other word than “automatically” or simply drop it.''}\\
\\
\textcolor{red}{We replace ``automatically'' by ``naturally'' in cases our conservation of quantities in our approach is addressed and by ``on-the-fly'' in the introduction of the revised manuscript.}\\
\\
\\
\textit{``l 199: only → Only''}\\
\\
\textcolor{red}{The sentence followed by ``only'' was accidentally left in the manuscript and is thus dropped in the revised manuscript.}

\section{Figures}

\textit{``Fig 1: increase size if axis labels\\
Fig 4: increase font size for text in the figure\\
Fig 5: increase font size for text in the figure\\
Fig 8: increase size of figures and font size of axis labels\\
Fig 9: increase size of figures and font size of axis labels''}\\
\\
\textcolor{red}{We follow the reviewer's request and increase the font sizes and sizes of the mentioned figures. For figures 8 and 9 we thus dropped the total time average which was anyway not discussed in the text.}

\section{Some suggestions and thoughts (optional) for a discussion section}

\textit{``Could “Flux Coupling” find its way into the title as both the exchange grid and the flux coupling are somewhat key here?''}\\
\\
We agree to the reviewer that both, the exchange grid and the flux coupling, are at the heart of the presented approach and thus both deserve to be part of the title.\\
\\
\textcolor{red}{We suggest to change the title of the revised manuscript as ``Flux coupling approach on an exchange grid for the IOW Earth System Model (version 1.04.00) of the Baltic Sea region''.}\\
\\
\\
\textit{``Couldn’t you finalise the conclusions with some strong point, e.g. what you gained and what has been significantly improved in your model? I find it a bit disappointing being told in the final message only what you would like to do in future.''}\\
\\
\textcolor{red}{We agree to the proposal of the reviewer and added a small paragraph at the end of the revised manuscript.}\\
\\
\\
\textit{``While the improvements you show are convincing I wonder how this relates to a model configuration where the atmosphere land ocean are run on identical grids – at the same resolution as your ocean?''}\\
\\
We want to draw the attention of the reviewer to the first two paragraphs of the introduction, where we address the feasibility of having the same grid for ocean and atmospheric models. As it is stated there, the Baltic Sea requires a particularly high spatial resolution due to the ``complicated coast lines given by numerous islands, narrow channels between the basins and the small baroclinic Rossby radius [...]. However, the corresponding atmospheric circulation is usually simulated on a much larger domain, since the pathways of cyclones originating from the North Atlantic region should be part of it. For this reason, the atmospheric model cannot be discretized with the same high resolution as the ocean model at reasonable numerical costs. ''. 
%
This was used as the motivation to develop a different strategy as it is carried out in the manuscript.\\
\\
\\
\textit{``As you couple albedo how would small scale processes, in particular clouds, would change the game if better resolved with a higher horizontal resolution of the atmosphere model. On the coarse atmosphere grid some horizontal averaging is done by construction.''}\\
\\
Investigations of such a kind will be done for the planned follow-up publication including the validation of the model with higher horizontal resolution of the atmospheric grid.\\
\\
\textcolor{red}{We add a sentence for giving this kind of outlook in Sect. \textit{3.1 Instability with atmospheric exchange grid} in the revised manuscript.}\\
\\
\\
\textit{``How would implicit coupling change the game. See e.g. Kang et al., 2021: Mass-conserving implicit–explicit methods for coupled compressible Navier–Stokes equations. https://doi.org/10.1016/j.cma.2021.113988 or Balaji et al., 2016: Coarse-grained component concurrency in Earth system modeling: parallelizing atmospheric radiative transfer in the GFDL AM3 model using the Flexible Modeling System coupling framework. https://doi.org/10.5194/gmd-9-3605-2016''}\\
\\
Both mentioned publications deal with the optimization of the coupling also within the employed model components and their respective source code. 
%
For instance, in Kang et al., 2021 it is proposed to advance one of the components (the ``stiffer one, i.e. the ocean) implicitly and the other (i.e. the atmosphere) explicitly.
%
In Balaji et al., 2016, the concurrent treatment of the radiation and the rest of the atmospheric time step is used to optimize the performance of the coupled model.
%
We currently see these strategies far beyond the scope of our manuscript, which introduces the flux coupling treated by an additional executable, the flux calculator, acting on an exchange grid.
%
One key point of the presented philosophy was to keep the involved model components and their source code as close to the original as possible, in order to permit flexibility in choosing these components.
%
A concrete investigation of how to optimize the overall performs of the coupled model will surely be a topic for future publication, where the work of Kang and Bajali will be certainly referenced.\\
\\
\\
\textit{``How does your approach relate to the one in the Bergen Model described by Furevik et al, 2003: Description and evaluation of the Bergen climate model: ARPEGE coupled with MICOM. https://doi.org/10.1007/s00382-003-0317-5''}\\
\\
The given reference by Furevik et al, 2003 presents an interesting strategy to overcome the same issues that are tackled by our approach.
%
The method is based on a Monte Carlo method, where a large number of points are distributed over both (oceanic and atmospheric) model domains.
%
The fraction of how many points that are contained in one atmospheric grid cell \textit{and} also in an underlying ocean model grid cell is then stored and used for the mapping of exchanged quantities.
%
To our understanding, this fraction would ultimately converge to the area fraction of one of our exchange grid cells and the linked atmospheric grid cell, if the number of random points approaches infinity.
%
Thus, we view the method presented in Furevik et al, 2003 as an alternative but equivalent implementation of the exchange grid.
%
However, it does not feature the calculation of the fluxes on the exchange grid.
%
Still, it is correcting fluxes on the higher resolved oceanic grid with the employed subgrid interpolation.
%
In how far this compares to our flux coupling is not so easily deducible.\\
\\
\textcolor{red}{We will refer to Furevik et al, 2003 in the introduction of the revised manuscript as an additional example of an exchange-grid-like approach.}  

\end{document}